--CL - úvery
WITH LOAN_PROD_INST as (
select
    'FCC' || '#' || 'CL' || '#' || acc.account_number as id_product_instance,
    'FCC' as id_source_system,
    'CL' as id_source_system_module,
    acp.market_prod_id as id_catalogue_product,
    acc.legdomcle as id_country_legal_domicile,
    acc.instsectr as id_institutional_sector,
    acc.nace as id_nace_economic_sector,
    acc.reltparty as id_related_party_type,
    acc.impstgnpl as id_impairment_stage_and_npl_category,
    acc.reportcls as id_reporting_class,
    acc.value_date as d_from,
    -- date_to - eventovy atribut datumu uzavretia uveroveho uctu
from   
FBI_OWN.FT_CLTB_ACCOUNT_APPS_MASTER acc
left join FBI_OWN.FT_CLTB_ACC_ADDL_FLDS_PROD_CUSTOM acp on acp.account_number = acc.account_number
),
PARTY_LOAN_PROD_INST_REL as (
select
    'FCC' || '#' || 'CL' || '#' || acc.account_number as id_product_instance,
    apt.customer_id as id_party,
    apt.responsibility as id_party_product_instance_relation_type 
    tab.mdm_code as id_party_product_instance_relation_type_master 
from FBI_OWN.FT_CLTB_ACCOUNT_APPS_MASTER acc
left join FBI_OWN.FT_CLTB_ACCOUNT_PARTIES apt on apt.account_number = acc.account_number
left join SI.SI_RDX_CD_TAB tab 
    on tab.id_mapping_definition = 1200101177
    and tab.src_id = apt.responsibility
    and tab.d_eff = &today
),
REF_TO_LOAN_PROD_INST as (
select 
    account_number as id_loan,
    'FCC' || '#' || 'CL' || '#' || account_number as id_product_instance
FROM FBI_OWN.FT_CLTB_ACCOUNT_APPS_MASTER
),
LOAN_PROD_INST_EXT_IDENTIFIER as (
select 
    'FCC' || '#' || 'CL' || '#' || acc.account_number as id_product_instance,
    acc.alt_ac_no as identifier,
    'SIP' as id_source_system
from FBI_OWN.FT_CLTB_ACCOUNT_APPS_MASTER acc
where alt_ac_no is not null    
)
;