WITH DEPO_PROD_INST as (
select 
    'FCC' || '#' || 'CA' || '#' || cust_ac_no as id_product_instance,
    'FCC' as id_source_system,
    'CA' as id_source_system_module,
    marketing_product_id_casa as id_catalogue_product,
    legdomcle as id_country_legal_domicile,
    instsectr as id_institutional_sector,
    nace as id_nace_economic_sector,
    reltparty as id_related_party_type,
    impstgnpl as id_impairment_stage_and_npl_category,
    reportcls as id_reporting_class,
    ac_open_date as date_from
    -- date_to - eventovy atribut uzavretia uctu 
from FBI_OWN.FT_STTM_CUST_ACCOUNT
),
PARTY_DEPO_PROD_INST_REL as (
select
    'FCC' || '#' || 'CA' || '#' || cust_ac_no as id_product_instance,
    cust_no as id_party,
    'XNA' as id_party_product_instance_relation_type,
    case when cust_no is not null then 'OWNER' end as id_party_product_instance_relation_type_master
from FBI_OWN.FT_STTM_CUST_ACCOUNT
),
REF_TO_DEPO_PROD_INST as (
select 
    cust_ac_no as id_deposit_account,
    'FCC' || '#' || 'CA' || '#' || cust_ac_no as id_product_instance
FROM FBI_OWN.FT_STTM_CUST_ACCOUNT
),
DEPO_PROD_INST_EXT_IDENTIFIER as (
select 
    'FCC' || '#' || 'CA' || '#' || acc.cust_ac_no as id_product_instance,
    acc.alt_ac_no as identifier,
    'SIP' as id_source_system
from FBI_OWN.FT_STTM_CUST_ACCOUNT acc
where alt_ac_no is not null
)
;