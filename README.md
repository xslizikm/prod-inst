# *Popis*
Produktová inštancia umožní unifikovaný pohľad na produkty, ktoré daný klient vlastní.

# *Návrh*
1. PK produktovej inštancie vyskladáme podla logiky TLA zdrojového systemu z ITAMu master systemu danej inštancie || '#' || module v zdrojovom systéme || '#' || PK v ODB + prefix/suffix, ak je potrebný 
2. Cieľom PK je prideliť jednoznačný identifikátor produktovej inštancii. Nemusí mať žiadny biznisový význam.
3. Produktovú inštanciu vždy zakladáme na základe dát zo systému, ktorý daný typ produktu vytvára, ak dostávame viacero inštancií daného záznamu, tak tieto inštancie referencujú jednu produktovú inštanciu
4. Ak máme k dispozícii viacero inštančných identifikátorov pre produktovú inštanciu, napríklad SIRECZ,K+, tak ich uložime do Product instance system identifier

# *Implementácia a popis riešenia pre jednotlivé domény*
## *Depozitá*
- Produktovú inštanciu zakladáme na granularite učtu v CASA module FCC (tabuľka STTM_CUST_ACCOUNT), číže ju zakladáme aj pre technické účty, ktoré slúžia na splácanie úverov, poprípade Loro/Nostro účty
- PK sa skladá: 'FCC' || '#' || 'CA' || '#' || cust_ac_no
### *Existujúce issues*
N/A
## *Lending*
- Produktovú inštanciu zakladáme na granularite úveru v CL module FCC (tabuľka CLTB_ACCOUNT_APPS_MASTER), zakladáme produktovú inštanciu aj pre poplatkový účet - produkt LCZA. Na ktorý sa vyrubujú poplatky limity
- PK sa skladá: 'FCC' || '#' || 'CL' || '#' || account_number
### *Existujúce issues*
1. Možno bude potrebné odfiltrovať poplatkový účet z produktovej inštancie
## *Letter of credit (Trade finance)*
- Produktovú inštanciu zakladáme na granularite kontraktu v LC module FCC (tabuľka CSTB_CONTRACT where module = 'LC', alebo posledná verzia kontraktu v LCTB_CONTRACT_MASTER)
- PK sa skladá: 'FCC' || '#' || 'LC' || '#' || le.contract_ref_no
### *Existujúce issues*
N/A
## *Bills and collections (Trade finance)*
- Produktovú inštanciu zakladáme na granularite kontraktu v BC module FCC (tabuľka CSTB_CONTRACT where module = 'BC', alebo posledná verzia kontraktu v BCTB_CONTRACT_MASTER)
- PK sa skladá: 'FCC' || '#' || 'BC' || '#' || le.bcrefno
### *Existujúce issues*
N/A
## *Limit*
- Produktovú inštanciu zakladáme na granularite záznamu v tabuľke GETM_FACILITY v ELCM module FCC.
- PK sa skladá: 'FCC' || '#' || 'GE' || '#' || fac.line_code || '#' || fac.line_serial || '#' || lia.liability_no as id_product_instance,
### *Existujúce issues*
1. ELCM sa nenachádza v číselniku modulov vo flexcube (tabuľka SMTB_MODULES). Momentálny workaround/riešenie je použíť ako modul GE (Generic interface), odvodený podľa názvu zdrojovej tabuľky.
2. Produktovú inštanciu zakladáme pre všetky limity a nie len pre tie, ktoré môžu mať na sebe priamo priviazaný produkt. Toto rozhodnutie nemame zvalidované od banky.
3. Pre shared liability este nie je naimplementované riešenie vo flexcube a nevieme odvodiť, kto je vlastníkom limitu (2020-08-10) -  https://jiravub:8443/browse/CHR-251
## *Treasury*
- Sekcia obsahuje súhrnné informácie ohladom treasury produktovej inštancie. Konkrétne detaily pre rôzne subdomény sa nachádzaju v príslušných podsekciách
- Všeobecný postup založenia produktovej inštancie pre treasury + vyplnenie referencií na ňu:
	1. Založíme na základe záznamu z master systému - K+. 
		1.1. PK v tvare 'KON' + modul z FCC +  Kondor ID s vynimkou pre FX, kde potrebujeme rozoznať SPOT/FORWARD/SWAP a MM, kde Kondor poskytuje zaznamy v 2 extraktoch.
	2. Vyplnenie referencií na produktovú inštanciu pre K+ záznamy
		2.1. Pre nove zaznamy (flag I) sa vyplni ref na produktovu instanciu. Pre ostatne zaznamy sa preberie zo vcerajsieho snapshotu a pre update je otvorena otazka ohladom toho ako sa ODB postavi k updatom? Ale viac menej ocakavam rovnaky postup ako pri inserte
	3. Vyplnenie referencií na produktovú inštanciu pre FCC záznamy
		3.1. Predpokladá sa, že kazdý FCC záznam bude mat vyplnenú referenciu na Kondor v atribúte CSTB_CONTRACT.EXTERNAL_REF_NO. FCC môže mať viacero záznamov pre 1 Kondor obchod. Všetky tieto záznamy budú referencovať rovnakú produktovú inštanciu.
	4. Vyplnenie Party-product instance relations
		4.1. GPID Counterparty zoberieme z najaktuálnejšiej verzie vo FCC.
- Impact na ODB - je potrebné mať pred loadom dostupné aktuálne dáta z FCC aj Kondoru
### *Existujúce issues*
1. Je z kondoru zabezpečená jedinečnosť PK medzi extraktami? poprípade v rámci jedneho extraktu v dlhšom časovom horizonte, čiže sa neprepoužívaju PK?
	1.1. PK nie je jedinečný v rámci kondor extraktov. Má to dopad na FX a MM, kde je viacero extraktov, ktoré plnia rovnakú tabuľku v ODB. Je potrebné pred kondor PK dať prefix/suffix
2. Pre presné mapovanie referencií na produktovú inštanciu potrebujeme vyriešiť akým spôsobom sa ODB postaví k Updatom z K+, či sa bude preberať z minulého dňa, alebo sa vytvorí záznam na novo. Druhú možnost by som preferoval a následne by sa k nim dalo pristúpiť rovnako ako k Insertom, s čím počíta návrh v nadsekcii	
### *Security deal (Treasury)*
- PK sa skladá : 'KON'||'#'|| 'SD' ||'#'|| kon.kondor_id pričom kondor_id je PK v K+
- Produktová inštanciu sa vytvára pre každý záznam z K+ v dnešnom snapshote v OC
#### *Existujúce issues*
N/A
### *OTC options (Treasury)*
- PK sa skladá : 'KON'||'#'|| 'OT' ||'#'|| kon.kondor_id pričom kondor_id je PK v K+
- - Produktová inštanciu sa vytvára pre každý záznam z K+ v dnešnom snapshote v OC
#### *Existujúce issues*
N/A
### *FX deal (Treasury)*
- PK sa skladá : 'KON'||'#'|| 'FX' ||'#'|| kon.kondor_id pričom kondor_id sa skladá z prefixu 'W2' - SWAP, 'SP' - SPOT, alebo 'FW' - FORWARD a PK daného K+ extraktu
- Produktovú inštanciu pre SWAP zakladajme z druhej nohy swapu (nachádzajú sa na nej informáci o dátume maturity druhej nohy)
- Produktová inštancia pre SPOT a FORWARD sa zakladá pre každý záznam v dnešnom snapshote v OC
#### *Existujúce issues*
N/A
### *Security Repo deal (Treasury)*
- PK sa skladá : 'KON'||'#'|| 'SR' ||'#'|| kon.repo_deal_id pričom repo_deal_id je PK v K+
- Produktová inštanciu sa vytvára pre každý záznam z K+ v dnešnom snapshote v OC
#### *Existujúce issues*
N/A
###  *Money market deal(Treasury)*
- PK sa skladá : 'KON'||'#'|| 'MM' ||'#'|| kon.kondor_id pričom kondor_id sa skladá z prefixu LD, alebo IM a id z konkrétných K+ extraktov obsahujúcich IAM a LD MM dealy
- Produktová inštanciu sa vytvára pre každý záznam z K+ v dnešnom snapshote v OC
#### *Existujúce issues*
N/A
### *Derivate deal (Treasury)*
N/A
### *Existujúce issues*
1. Deriváty nie sú naparametrizované a pre CZ by sa nemali používať. Preto zatiaľ produktovú inštanciu nezakladáme.

# *Súvisiace JIRA tickety*
RIS-1003 - slúží ako zberný kôš pre issues týkajúcich sa produktovej inštancie
RIS-933 - Potrebujeme do CD_TAB dostať preklad pre LC,BC a CL moduly