--BC module - Trade finance
with LAST_EVENT as (
select 
    bc.*,
    row_number() over (partition by bcrefno order by event_seq_no desc) rn 
from FBI_OWN.FT_BCTB_CONTRACT_MASTER bc
),
BC_PROD_INST as (
select
    'FCC' || '#' || 'BC' || '#' || le.bcrefno as id_product_instance,
    'FCC' as id_source_system,
    'BC' as id_source_system_module,
    le.marketing_product_id_tf as id_catalogue_product,
    le.legdomcle as id_country_legal_domicile,
    le.instsectr as id_institutional_sector,
    le.nace as id_nace_economic_sector,
    le.reltparty as id_related_party_type,
    le.impstgnpl as id_impairment_stage_and_npl_category,
    le.reportcls as id_reporting_class,
    le.value_date as date_from,
    le.maturity_date as date_to
from
LAST_EVENT le 
where le.rn = 1
),
PARTY_BC_PROD_INST_REL as (
select
    'FCC' || '#' || 'BC' || '#' || le.bcrefno as id_product_instance,
    cpt.party_id as id_party,
    cpt.party_type as id_party_product_instance_relation_type
    tab.mdm_code as id_party_product_instance_relation_type_master 
from
LAST_EVENT le 
left join FBI_OWN.FT_BCTB_CONTRACT_PARTIES cpt on cpt.bcrefno = le.bcrefno and cpt.event_seq_no = le.event_seq_no
left join SI.SI_RDX_CD_TAB tab 
    on tab.id_mapping_definition = 1200101177
    and tab.src_id = cpt.party_type
    and tab.d_eff = &today
where le.rn = 1
),
REF_TO_BC_PROD_INST as (
select
    le.bcrefno  as id_bills_and_collections_contract,
    'FCC' || '#' || 'BC' || '#' || le.bcrefno as id_product_instance
from LAST_EVENT le
where le.rn=1    
),
BC_PROD_INST_EXT_IDENTIFIER as (
select
    'FCC' || '#' || 'BC' || '#' || le.bcrefno as id_product_instance,
    cnt.user_ref_no as identifier,
    'SIP' as id_source_system
from LAST_EVENT le
inner join FBI_OWN.FT_CSTB_CONTRACT cnt on cnt.contract_ref_no = le.bcrefno
where cnt.user_ref_no is not null
and le.rn = 1    
)
;